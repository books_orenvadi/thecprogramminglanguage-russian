// #include "./template.typ"
#import "./template.typ": *

#show: template.with()


#include "./chapters/0-cover.typ" // done
#include "./chapters/1-preface.typ" // done
#include "./chapters/2-intro.typ" // done
#include "./chapters/3-lang-review.typ" // done
#include "./chapters/4-type-ops-exprs.typ" // done
