#let template(body) = {
  // page settings
  set page(
    paper: "iso-b5",
    margin: (left: 10mm, right: 10mm, top: 10mm, bottom: 10mm),
    numbering: "1",
  )
  set text(lang: "ru", font: "Calibri")

  // New chapter starts with new page
  show heading.where(level: 1): it => {
    pagebreak()
    it
    par()[#text(size:0.5em)[#h(0.0em)]]
  }
  show heading: it => {
    it
    par()[#text(size:0.5em)[#h(0.0em)]]
  }

  set par(justify: true, first-line-indent: 0.40cm)


  show heading.where(level: 1, outlined: true, numbering: "1."): set heading(
    numbering: 
      (..nums) => "Глава " + nums
      .pos()
      .map(str)
      .join(".") + "."
  )

  show heading.where(level: 1): set text(size: 24pt, fill: rgb("#365f91"))
  show heading.where(level: 2): set text(size: 18pt, fill: rgb("#4f81bd"))
  show heading.where(level: 3): set text(size: 15pt, fill: rgb("#4f81bd")) 

  show heading: set text(font: "XCharter")
  set heading(numbering: "1.")


  show outline.entry.where(
    level: 1
  ): it => {
    v(15pt, weak: true)
    strong(it)
  }

  show outline.entry.where(
    level: 2
  ): it => {
    h(10pt)+it
  }

  show outline.entry.where(
    level: 3
  ): it => {
    h(20pt)+it
  }


  show raw.where(block: false): set text(fill: rgb("#0070c0"))

  show raw.where(block: true): block.with(
    width: 96%,
    fill: luma(245),
    inset: 10pt,
    radius: 4pt,
  )

  show raw.where(block: true): it => {
    it
    par()[#text(size:0.5em)[#h(0.0em)]]
  }


  body

}
