#v(25pt)

#set par(justify: false)
#align(center)[

  #text(size: 24pt, font: "Linux Libertine", weight: "bold", "3-е издание")

  #text(size: 35pt, font: "Linux Libertine", "Язык программирования Си")

  #line(length: 100%, stroke: 0.14mm + blue)


  #text(size: 25pt, font: "Linux Libertine", "Брайан Керниган, Деннис Ритчи")

]

#v(55pt)

#grid(
  columns: (110pt, auto),
  h(110pt),
  image("./images/theclogoansi.svg", width: 100%)
)

#outline(depth: 3)

